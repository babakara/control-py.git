#frenet frame 和 cartesian转换

import numpy as np
from math import *


def cartesian_to_frenet1D(rs, rx, ry, rtheta, x, y):
	# 笛卡尔坐标转frenet坐标 参考轨迹为T轴
	# x y 转到 s l
	s_condition = np.zeros(1)
	d_condition = np.zeros(1)

	dx = x - rx
	dy = y - ry

	cos_theta_r = cos(rtheta)
	sin_theta_r = sin(rtheta)

	cross_rd_nd = cos_theta_r * dy - sin_theta_r * dx
	d_condition[0] = copysign(sqrt(dx * dx + dy * dy), cross_rd_nd)

	s_condition[0] = rs

	return s_condition, d_condition


def frenet_to_cartesian1D(rs, rx, ry, rtheta, s_condition, d_condition):
	# frenet坐标转笛卡尔坐标
	# s l 转到 x y
	if fabs(rs - s_condition[0]) >= 1.0e-6:
		print("The reference point s and s_condition[0] don't match")

	cos_theta_r = cos(rtheta)
	sin_theta_r = sin(rtheta)

	x = rx - sin_theta_r * d_condition[0]
	y = ry + cos_theta_r * d_condition[0]

	return x, y


def cartesian_to_frenet2D(rs, rx, ry, rtheta, rkappa, x, y, v, theta):
	# 笛卡尔坐标转frenet坐标 参考轨迹为T轴

	s_condition = np.zeros(2)
	d_condition = np.zeros(2)

	dx = x - rx
	dy = y - ry

	cos_theta_r = cos(rtheta)
	sin_theta_r = sin(rtheta)

	cross_rd_nd = cos_theta_r * dy - sin_theta_r * dx
	d_condition[0] = copysign(sqrt(dx * dx + dy * dy), cross_rd_nd)

	delta_theta = theta - rtheta
	tan_delta_theta = tan(delta_theta)
	cos_delta_theta = cos(delta_theta)

	one_minus_kappa_r_d = 1 - rkappa * d_condition[0]
	d_condition[1] = one_minus_kappa_r_d * tan_delta_theta

	s_condition[0] = rs
	s_condition[1] = v * cos_delta_theta / one_minus_kappa_r_d

	return s_condition, d_condition

def frenet_to_cartesian2D(rs, rx, ry, rtheta, rkappa, s_condition, d_condition):
	if fabs(rs - s_condition[0]) >= 1.0e-6:
		print("The reference point s and s_condition[0] don't match")

	cos_theta_r = cos(rtheta)
	sin_theta_r = sin(rtheta)

	x = rx - sin_theta_r * d_condition[0]
	y = ry + cos_theta_r * d_condition[0]

	one_minus_kappa_r_d = 1 - rkappa * d_condition[0]
	tan_delta_theta = d_condition[1] / one_minus_kappa_r_d
	delta_theta = atan2(d_condition[1], one_minus_kappa_r_d)
	cos_delta_theta = cos(delta_theta)

	theta = NormalizeAngle(delta_theta + rtheta)

	d_dot = d_condition[1] * s_condition[1]

	v = sqrt(one_minus_kappa_r_d * one_minus_kappa_r_d * s_condition[1] * s_condition[1] + d_dot * d_dot)

	return x, y, v, theta


# alpha = np.arctan2(dnewy, dnewx)
# kappa = (ddnewx*dnewy-ddnewy*dnewx)/(dnewx*dnewx+dnewy*dnewy)**(3.0/2.0)
# norm to [-pi, pi]
def NormalizeAngle(angle):
	a = fmod(angle+np.pi, 2*np.pi)
	if a < 0.0:
		a += (2.0*np.pi)
	return a - np.pi


def polyfit(coeffs, t, order):
	if order == 0:
		return coeffs[0] + coeffs[1] * t + coeffs[2] * t * t + coeffs[3] * t * t * t
	if order == 1:
		return coeffs[1] + 2 * coeffs[2] * t + 3 * coeffs[3] * t * t
	if order == 2:
		return 2 * coeffs[2] + 6 * coeffs[3] * t
	if order == 3:
		return 6 * coeffs[3]
	else:
		return 0.0


def cubicPolyCurve1d(x0, dx0, x1, dx1, T):
	coeffs = np.zeros(4)
	coeffs[0] = x0
	coeffs[1] = dx0

	T2 = T * T

	coeffs[2] = (3 * x1 - T * dx1 - 3 * coeffs[0] - 2 * coeffs[1] * T) / T2
	coeffs[3] = (dx1 - coeffs[1] - 2 * coeffs[2] * T) / (3.0 * T2)
	return coeffs
def ComputeCurvature(dx, ddx, dy, ddy):
	a = dx*ddy - dy*ddx
	norm_square = dx*dx+dy*dy
	norm = sqrt(norm_square)
	b = norm*norm_square
	return a/b

def ComputeCurvatureDerivative(dx, ddx, dddx, dy, ddy, dddy):
	a = dx*ddy-dy*ddx
	b = dx*dddy-dy*dddx
	c = dx*ddx+dy*ddy
	d = dx*dx+dy*dy
	return (b*d-3.0*a*c)/(d*d*d)

def CalculateTheta(rtheta, rkappa, l, dl):
	return NormalizeAngle(rtheta + atan2(dl, 1-l*rkappa))

def CalculateKappa(rkappa, rdkappa, l, dl, ddl):
	denominator = (dl * dl + (1 - l * rkappa) * (1 - l * rkappa))
	if fabs(denominator) < 1e-8:
		return 0.0
	denominator = pow(denominator, 1.5)
	numerator = (rkappa + ddl - 2 * l * rkappa * rkappa -
						   l * ddl * rkappa + l * l * rkappa * rkappa * rkappa +
						   l * dl * rdkappa + 2 * dl * dl * rkappa)
	return numerator / denominator

# 测试部分 move to pose

