import os
def get_count_codelines(dir_path):  #定义统计代码行的函数
    file_count = 0
    all_line_count = 0
    empty_line_count = 0
    comment_line_count = 0
    for root, dirs, files in os.walk(dir_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path[-3:] == ".py":     #判断是否为python代码文件
                file_count += 1     #统计文件个数
                with open(file_path, 'r', encoding="utf-8") as fp:
                    for line in fp:
                        all_line_count += 1      #统计所有的行数
                        if line.strip() == "":
                            empty_line_count += 1       #统计空行数
                        if line[0] == "#":
                            comment_line_count += 1     #统计注释行数
    return (file_count,all_line_count,empty_line_count,comment_line_count)


if __name__ == "__main__":
    
    _,a_lines,_,_ = get_count_codelines("D:\QT\control-py\algo_struct")
    
    _,b_lines,_,_ = get_count_codelines("D:\QT\control-py\disturbance")
    
    _,c_lines,_,_ = get_count_codelines("D:\QT\control-py\dynamics_simulation")
    
    _,d_lines,_,_ = get_count_codelines("D:\QT\control-py\navigation")
    
    _,e_lines,_,_ = get_count_codelines("D:\QT\control-py\out_interface")
    
    _,f_lines,_,_ = get_count_codelines("D:\QT\control-py\path_plan")
    
    all_lines = a_lines + b_lines + c_lines + d_lines + e_lines + f_lines
    
    print(all_lines)
    