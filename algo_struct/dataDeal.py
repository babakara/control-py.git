# 数据处理入口

import csv
from scipy.spatial import ConvexHull
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.font_manager as fm

# readfile
# get data
# convex
# plot

fileName = 'D:/702/无人艇/PZ数据/2020_12_16太湖试验.csv'
dict = {'pch':[]}
with open(fileName, "rt") as csvfile:
    # reader = csv.reader(csvfile)
    reader = csv.DictReader(csvfile)
    for row in reader:
        # column = [row[2] for row in reader]
        # print(row['pch'], row['rll'])
        dict['pch'].append((float(row['pch']),float(row['rll'])))
        # print(column)

points = dict['pch']
points_array = np.array(points)
print(len(points_array))
hull = ConvexHull(points_array)
# print(result)
# result_array = np.array(result)
# hull.vertices 得到凸轮廓坐标的索引值，逆时针画
hull1=hull.vertices.tolist()#要闭合必须再回到起点[0]
hull1.append(hull1[0])

plt.rcParams["font.sans-serif"]=["SimHei"] #设置字体
plt.rcParams["axes.unicode_minus"]=False #该语句解决图像中的“-”负号的乱码问题
#设置字体为楷体
plt.xlim([-20,20])
plt.ylim([-20,20])
plt.xlabel('横摇角/deg')
plt.ylabel('俯仰角/deg')
plt.title('无人艇耦合摇摆范围')

plt.plot(points_array[hull1,0], points_array[hull1,1], 'r--^',lw=2)

plt.plot(points_array[:,0], points_array[:,1], linestyle=' ', marker='o')
for i in range(len(hull1)-1):
    plt.text(points_array[hull1[i],0], points_array[hull1[i],1],str(points_array[hull1[i]]),fontsize=10)
plt.pause(0)