import configparser
# 水动力系数结构体
import numpy as np
from out_interface import fileDeal
# 自动迭代参数
filename = '../out_interface/set.ini'
# 外部手动修改参数
extern_file = '../out_interface/ini/refer.ini'
apf_section = 'APFCoe'


# 外部手动修改的水动力参数
boat_file = 'out_interface/boat.ini' #相对路径读不到文件
boat_type = ('boata','boatb')
myboat_name = boat_type[0]
data_file = 'dynamics_simulation/data.csv'

class MyDynCoe:
	'''
	水动力系数类 保存水动力系数变量
	'''
	def __init__(self):

		self.rho_w = 1000.0
		self.g_e = 9.81
		self.x_g = 0.0
		self.y_g = 0.0
		self.z_g = 0.0
		self.x_b = 0.0
		self.y_b = 0.0
		self.z_b = 0.3
		self.l = 2.38 #船体特征长度

		self.m = 127.0 # 船体质量
		self.ine_xx = 15.73
		self.ine_xy = 0.0
		self.ine_yy = 70.27
		self.ine_zz = 71.38
		self.ine_xz = 0.0
		self.ine_yz = 0.0

		# hydrodynamic coefficient
		self.x_u_ = -4.8e-3
		self.x_uu = -9.3e-3
		self.y_r_ = -9.67e-4
		self.y_v_ = -3.66e-2
		self.y_r = -4.8e-3
		self.y_v = -5.69e-2
		self.y_d_r = -2.15e-2
		self.z_q_ = -1.86e-4
		self.z_w_ = -6.8e-2 #吃水变化会影响水动力系数
		# self.z_q = -7.61e-2 # 由于风浪大了和时间长了还是会发散 因此又去掉了几个耦合水动力系数
		self.z_q = 7.61e-3
		self.z_w = -8.3e-3 # 需要该项才能收敛
		# self.z_w = .0

		# self.z_d_s = -3.3e-2
		self.z_d_s = .0
		self.k_p_ = 0# -4次左右对称，-5/-6左右不对称
		self.k_p = 0
		self.k_vq = 0.0
		self.k_wr = 0.0
		self.m_q_ = -4.1e-4
		self.m_w_ = -1.82e-4
		self.m_q = -2.07e-3
		# self.m_q = .0
		# self.m_w = 3.72e-3 # 现在只有这个系数有问题 导致发散 符号还是量级？
		# self.m_w = 3.72e-5
		self.m_w = .0
		self.m_d_s = 2.13e-3
		# self.m_d_s = .0
		self.n_r_ = -2.4e-3
		self.n_v_ = 9.54e-4
		self.n_r = -0.0664
		self.n_rr= -0.0054
		self.n_v = -1.86e-2
		self.n_vv = 0.0
		# self.n_v = .0
		self.n_d_r = 0.0829
		# 系数参照C++ pz-m.txt

	def boat_select(self, filename,boat_name):
		# 新尺度的船只
		config = fileDeal.readIni(filename)
		self.rho_w = float(config.get(boat_name, 'rho_w')) #海水平均密度
		self.l = float(config.get(boat_name, 'l')) #长度
		self.m = float(config.get(boat_name, 'm'))#kg 质量
		self.ine_xx = float(config.get(boat_name, 'ine_xx'))#kg/m^3
		self.ine_yy = float(config.get(boat_name, 'ine_yy'))
		self.ine_zz = float(config.get(boat_name, 'ine_zz'))
		self.k_p_ = float(config.get(boat_name, 'k_p_'))
		self.k_p = float(config.get(boat_name, 'k_p'))
		self.m_q_ = float(config.get(boat_name, 'm_q_'))
		self.m_q = float(config.get(boat_name, 'm_q'))

class APFCoe:
	# 放射斥力系数
	# 误差规则loss-> 反馈规则
	goal_k = 20
	# 手动调参，引力系数 如果仿真超时, 就小幅增加引力
	obj_eta = 1e9
	# 手动调参, 斥力系数 配置迭代方法, 每进行一次仿真计算的所有障碍的最短距离, 如果有小于危险半径的, 累积, 按比例小幅增加斥力, 指导逼近
	obj_r = 2.5 * 10
	# 斥力场范围 取障碍物危险半径的n倍 每进行一次仿真计算, 就统计所有的避障情况, 如果有小于危险半径的, 累积, 增加斥力场范围
	c_obj = 5e-2
	goal = np.zeros(3, dtype=float)
	goal_r = 50
	# 到点半径
	end_dis = 6


class ObjectInfo:
	x = np.zeros(6, dtype=float)
	nu = np.zeros(6, dtype=float)
	lhw = np.zeros(3, dtype=float)
	danger_r = 30
	type = 'none'


class ControlParams:
	# 船体状态和驱动
	def __init__(self):
		
		self.x = np.zeros(6, dtype=float)
		self.u = np.zeros(6, dtype=float)
		self.nu = np.zeros(6, dtype=float)
		self.f = np.zeros(6, dtype=float)
		self.delta = np.zeros(3, dtype=float)
		# 期望速度
		self.target_u = 2
		# 减速速度
		self.low_u = 1
		# 增量PID控制用
		self.pidu = np.zeros(3, dtype=float)
		self.oldedx = np.zeros(3, dtype=float)
		self.old2edx = np.zeros(3, dtype=float)
		# 跟踪任务完成情况 0--False 1--True
		self.flag = 0
		# 任务模式 1--跟踪  2--避障
		self.mode = 1
		# 驱动方式 1--舵机驱动 2--差速驱动
		self.f_mode = 1
		self.boat_name = 'boat_a'#船的类型
		self.delta_max = 35 * np.pi / 180.0
		self.F_max = 300 # 推力轉速一一對應
		self.M_max = 300

	def boat_select(self, filename,boat_name):
		config = fileDeal.readIni(filename)
		self.boat_name = boat_name
		self.delta_max = float(config.get(boat_name, 'delta_max')) *np.pi/180.0
		self.F_max = float(config.get(boat_name, 'F_max'))
		self.M_max = float(config.get(boat_name, 'M_max'))

def ini_read(name):
	'''
	读取配置文件并返回若干结构体
	:param name: 字符串 配置文件名
	:return: APFCoe类型
	'''
	config = configparser.ConfigParser()
	config.read(name)
	coe = APFCoe()
	coe.goal_k = config.get(apf_section, 'goal_k')
	coe.obj_eta = config.get(apf_section, 'obj_eta')
	coe.obj_r = config.get(apf_section, 'obj_r')
	coe.c_obj = config.get(apf_section, 'c_obj')
	return coe


def ini_write(name, coe):
	config = configparser.ConfigParser()
	config.read(name)
	if config.has_section(apf_section):
		print(apf_section, ' has exist')
	else:
		config.add_section(apf_section)
	config.set(apf_section, 'goal_k', str(coe.goal_k))
	config.set(apf_section, 'obj_eta', str(coe.obj_eta))
	config.set(apf_section, 'obj_r', str(coe.obj_r))
	config.set(apf_section, 'c_obj', str(coe.c_obj))

	# 写入配置文件
	config.write(open(filename, 'w'))

# -------------------------------------TEST--------------------------------
# cc = APFCoe()
# ini_write(filename, cc)
