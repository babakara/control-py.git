import csv
import threading
from numpy import cos, sin, tan
import numpy as np
from disturbance import wave, wind
from path_plan import pathPlan
from algo_struct.algo import rad_limit
from algo_struct.myStruct import MyDynCoe, ControlParams, APFCoe,boat_file,data_file,myboat_name
from collections import namedtuple

# 运动仿真
def sec(angle: float) -> float:
    '''
    sec三角函数计算
    :param angle:弧度制
    :return: 返回计算结果或者报错
    '''
    try:
        return 1 / cos(angle)
    except OSError as error_msg:
        print(error_msg)
        print("angle is zero!")


def uuv_b2n(u, x):
    '''
    坐标转换函数, {B}到{N}
    :param u:状态6
    :param x:位置6
    :return:转换后的状态nu 6
    '''
    phi = x[3]
    theta = x[4]
    psi = x[5]
    trans = np.array([
        [cos(psi) * cos(theta), cos(psi) * sin(theta) * sin(phi) - sin(psi) * cos(phi),
         cos(psi) * sin(theta) * cos(phi) + sin(psi) * sin(phi), 0, 0, 0],
        [sin(psi) * cos(theta), sin(psi) * sin(theta) * sin(phi) + cos(psi) * cos(phi),
         sin(psi) * sin(theta) * cos(phi) - cos(psi) * sin(phi), 0, 0, 0],
        [-sin(theta), cos(theta) * sin(phi), cos(phi) * cos(theta), 0, 0, 0],
        # [-sin(theta), cos(theta) * sin(phi), cos(phi) * cos(theta), 0, 0, 0],
        [0, 0, 0, 1, sin(phi) * tan(theta), cos(phi) * tan(theta)],
        [0, 0, 0, 0, cos(phi), -sin(phi)],
        [0, 0, 0, 0, sin(phi) * sec(theta), cos(phi) * sec(theta)]
    ])

    return np.dot(trans, u)

boat_status = namedtuple('Position', ('x', 'y', 'z', 'p', 'q', 'r'))
boat_velocity = namedtuple('Velocity', ('dx', 'dy', 'dz', 'dp', 'dq', 'dr'))


def unliner_kt(K, T, delta, alpha, deltam, x):
    dx1 = x[1]
    dx2 = (K*(deltam + delta) - x[1] - alpha* (x[1]**3))/T
    return np.array([dx1, dx2]) 

def deg_limit(deg):
	# 0~2pi to -pi~pi
	deg = deg - 360 * np.floor(deg / 360)
	if deg >= 180:
		deg = deg - 2 * 180
	if deg < -180:
		deg = deg + 2 * 180
	return deg

class BoatMotionSim():
    '''
    运动仿真线程类--船舶
    调用后生成运动轨迹并绘图

    '''

    def __init__(self, delta_h:float, K:float, T:float, alpha:float, deltam: float) -> None:
        '''
        :param total_time: 仿真参数设置:总的仿真时间float
        '''
        super().__init__()
        self.boat = MyDynCoe()
        self.boat.boat_select(filename=boat_file,boat_name=myboat_name)#选择不同尺度的船
        self.wave = wave.MyDynWave()
        self.wave.boat_select(filename=boat_file,boat_name=myboat_name)
        self.wind = wind.myDynWind()
        self.wind.boat_select(filename=boat_file,boat_name=myboat_name)
        self.h = delta_h
        
        self.K = K
        self.T = T
        self.alpha = alpha
        self.deltam = deltam

    def mmg_equ(self,
                u: np.ndarray,
                x: np.ndarray,
                delta: float,
                f: float ):
        '''
        动力学方程
        :param u:
        :param x:
        :param delta:
        :param f:
        :return:
        '''
        if len(u) == 6 and len(x) == 6:
            phi = x[3]
            theta = x[4]
            psi = x[5]
            delta_r = delta

            uu = u[0]
            v = u[1]
            w = u[2]
            p = u[3]
            q = u[4]
            r = u[5]

            l_2 = 0.5 * self.boat.rho_w * (self.boat.l ** 2)
            l_3 = 0.5 * self.boat.rho_w * (self.boat.l ** 3)
            l_4 = 0.5 * self.boat.rho_w * (self.boat.l ** 4)
            l_5 = 0.5 * self.boat.rho_w * (self.boat.l ** 5)

            # 惯量和附加质量矩阵
            m_matrix = np.array([
                [self.boat.m - l_3 * self.boat.x_u_, 0, 0, 0, 0, 0],
                [0, self.boat.m - l_3 * self.boat.y_v_, 0, 0, 0, -l_4 * self.boat.y_r_],
                [0, 0, self.boat.m - l_3 * self.boat.z_w_, 0, -l_4 * self.boat.z_q_, 0],
                [0, 0, 0, self.boat.ine_xx - l_5 * self.boat.k_p_, 0, 0],
                [0, 0, -l_4 * self.boat.m_w_, 0, self.boat.ine_yy - l_5 * self.boat.m_q_, 0],
                [0, -l_4 * self.boat.n_v_, 0, 0, 0, self.boat.ine_zz - l_5 * self.boat.n_r_]
            ])

            midVar = np.sqrt(v**2 + w ** 2) # 不能除0

            # midDiv = 0.0
            # if v != 0:
            #     midDiv = abs(r / v)

            # 水动力矩阵 参考1979公式
            # c_matrix = np.array([
            #     [l_2 * self.boat.x_uu * uu, 0, 0, 0, self.boat.m * w, -self.boat.m * v],
            #     [l_3 * self.boat.y_r * r + l_3 * self.boat.y_v * v, 0, 0, -self.boat.m * w, 0, self.boat.m * uu],
            #     [l_3 * self.boat.z_q * q + l_3 * self.boat.z_w * w, 0, 0, self.boat.m * v, -self.boat.m * uu, 0],
            #     [l_4 * self.boat.k_p, l_4 * self.boat.k_vq * q, l_4 * self.boat.k_wr * r, 0, self.boat.ine_zz * r,
            #      -self.boat.ine_yy * q],
            #     [l_4 * self.boat.m_q * q + l_3 * self.boat.m_w * w, 0, 0, -self.boat.ine_zz * r, 0,
            #      self.boat.ine_xx * p],
            #     [l_4 * self.boat.n_r * r + l_4 * self.boat.n_v * v, l_3 * self.boat.n_vv*midVar, 0, self.boat.ine_yy * q, -self.boat.ine_xx * p,
            #      l_5 * self.boat.n_rr * abs(r)]
            # ])
            
            c_matrix = np.array([
                [l_2 * self.boat.x_uu * uu, 0, 0, 0, -self.boat.m * w, self.boat.m * v],
                [l_3 * self.boat.y_r * r + l_3 * self.boat.y_v * v, 0, 0, self.boat.m * w, 0, -self.boat.m * uu],
                [l_3 * self.boat.z_q * q + l_3 * self.boat.z_w * w, 0, 0, -self.boat.m * v, self.boat.m * uu, 0],
                [l_4 * self.boat.k_p*p, l_4 * self.boat.k_vq * q, l_4 * self.boat.k_wr * r, 0, -self.boat.ine_xx * r,
                 self.boat.ine_yy * q],
                [l_4 * self.boat.m_q * q + l_3 * self.boat.m_w * w, 0, 0, self.boat.ine_zz * r, 0,
                 -self.boat.ine_xx * p],
                [l_4 * self.boat.n_r * r + l_4 * self.boat.n_v * v, l_3 * self.boat.n_vv*v*abs(midVar), 0, -self.boat.ine_yy * q, self.boat.ine_xx * p,
                 l_5 * self.boat.n_rr * abs(r)]
            ])

            # 风浪流参数更新
            # self.wave_param_init()
            # tau_wave = self.wave_force_cal(self.t)
            self.wave.wave_param_init(self.control)
            tau_wave = self.wave.wave_force_cal(self.t, self.control)
            # print('波浪力: ', tau_wave)

            G = self.wave.C * self.wave.d * self.wave.B * self.wave.l * self.wave.rho_w * self.wave.g_e
            B = (x[2] + self.wave.d) * self.wave.C * self.wave.B * self.wave.l * self.wave.rho_w * self.wave.g_e
            if B < 0:
                B = 0  # 没有吃水了 浮力就是0 不能变方向

            if x[2] < -self.wave.d:
                # 如果吃水太浅 就暂时不计入波浪扰动力
                tau_wave = np.zeros(6)
            B_con = self.wave.rho_w * self.wave.g_e * self.wave.d * self.wave.C * self.wave.B * self.wave.l

            # 船体重力矢量 前三个是力，沿船头X轴，船侧Y轴，船底Z轴，后三个是力矩，绕船头X轴，绕船侧Y轴，绕船底Z轴 单位N和N*m
            g_matrix_u = np.array([
                0,
                0,
                (G - B),  # 浮力抵消垂向波浪力和重力 使用线性化理论 简化公式
                -B_con * sin(phi) * self.wave.GMt,
                -B_con * sin(theta) * self.wave.GMl,
                0
            ])

            # 船体舵力和力矩6*1 含义同重力
            rudder = np.array([
                0,
                l_2 * self.boat.y_d_r * (uu ** 2) * delta_r,
                0,
                # l_2 * self.boat.z_d_s * (uu ** 2) * delta_s,
                0,
                # l_3 * self.boat.m_d_s * (uu ** 2) * delta_s,
                0,
                l_3 * self.boat.n_d_r * (uu ** 2) * delta_r
            ])

            # 风力6*1 含义同重力
            tau_wind = self.wind.blendermann(u, psi)
            tau = f + tau_wave + tau_wind

            #out 是解动力学方程得到的加速度
            out = np.dot(np.linalg.inv(m_matrix), np.dot(c_matrix, u) + rudder + tau + g_matrix_u)
            # print('out', out)
            return out
        

    def kt(self,                 
            u: np.ndarray,
            x: np.ndarray,
            delta: float):
        
        psi = x[5]
        dpsi = u[5]
        
        temp = np.array([psi, dpsi])
        
        k1 = unliner_kt(self.K, self.T, delta, self.alpha, self.deltam, temp)
        k1_ = temp + 0.5*self.h*k1
        k2 = unliner_kt(self.K, self.T, delta, self.alpha, self.deltam, k1_)
        k2_ = temp + 0.5*self.h*k2
        k3 = unliner_kt(self.K, self.T, delta, self.alpha, self.deltam, k2_)
        k3_ = temp + self.h*k3
        k4 = unliner_kt(self.K, self.T, delta, self.alpha, self.deltam, k3_)

        temp = temp + (self.h/6)*(k1 + 2*k2 + 2*k3 + k4)
        temp[0] = deg_limit(temp[0]) # 限制角度在-pi~pi
        
        # 转速 对应 航速
        return temp
        

    def runMMG(self, x: np.ndarray, u: np.ndarray, delta:float, f:float) -> None:
        k1 = self.mmg_equ(u=u, x=x, delta=delta, f=f)
        k2 = self.mmg_equ(u=u + 0.5 * self.h * k1, x=x, delta =delta, f=f)
        k3 = self.mmg_equ(u=u + 0.5 * self.h * k2, x=x, delta =delta, f=f)
        k4 = self.mmg_equ(u=u + self.h * k3, x=x, delta=delta, f=f)
        u = u + (self.h / 6) * (k1 + 2 * k2 + 2 * k3 + k4)

        nu = uuv_b2n(u, x)
        # current_speed = np.sqrt(nu[0]**2 + nu[1]**2)
        x += nu*self.h

        print('位置数据x:', x) #存入文件是更好的方式
        print('有义波高hs:', self.wave.wave_Hs) #波高

        return x, nu

    # 计算角度和角速度 单位 deg deg/s
    def runKt(self, x: np.ndarray, u: np.ndarray, delta:float, f:float):

        print(f'u is {u}')
        temp = self.kt(u=u, x=x, delta=delta)
        
        u[0] = f*0.001*np.sin(temp[0]) # 北速度
        u[1] = f*0.001*np.cos(temp[1]) # 东速度
        u[5] = temp[1]
        x += u*self.h
        x[5] = temp[0]
        
        print('x:', x) #存入文件是更好的方式
        return x, u