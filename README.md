# controlPy

#### 介绍

脚本功能的介绍，见.\reference\船体运动仿真.docx

![boata](https://foruda.gitee.com/images/1677392631165492846/25661c77_5303604.png)

上图使用了.\out_interface\boat.ini中[boat_a]船进行路径跟踪。由于引入了风浪干扰，因此存在垂荡Z和roll pitch变化，并用曲线展示。

你可以按你的喜好改变船型为[boat_b]。这样的话，你需要对应修改.\path_plan\pathPlan.py文件，对uuv_s中的控制参数进行调参或是换为其他控制器。

#### 安装教程

1.  git clone https://gitee.com/babakara/control-py.git

#### 使用说明

1.  使用frenet分支；
2.  main.py调用跟踪仿真；
3.  boat.ini是参数配置文件，如果有相关报错，首先检查绝对路径并修改。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
