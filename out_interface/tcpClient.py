import queue
import threading
import time
import socket
import matplotlib.pyplot as plt
import numpy as np
from algo_struct.myStruct import ObjectInfo
from navigation import gpsAndUtm
from dynamics_simulation import motionSim
from algo_struct.algo import my_interpolate

# 和工控机服务器通信
# 每次连接成功 发送身份识别字符串 pathPlan
# 断线自动重连  服务器没上线等待


class MyTcpClient(threading.Thread):

	def __init__(self, get_port, max_len) -> None:
		super().__init__()
		self.identify = "pathPlan"
		# 创建 socket 对象
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# 获取本地主机名
		self.host = socket.gethostname()
		# 设置端口号
		self.port = get_port
		self.maxLen = max_len
		self.cnt_flag = False
		self.msg = ""

	def start_connect(self):
		'''
		开始连接
		'''
		while not self.cnt_flag:
			# 连接服务，指定主机和端口
			time.sleep(1)
			# self.s.connect((self.host, self.port))
			# self.__cntFlag = True
			try:
				self.s.connect((self.host, self.port))
				print("连接成功")
				self.cnt_flag = True
			except ConnectionRefusedError:
				self.cnt_flag = False
				print("等待服务器上线")
			except OSError:
				# 创建 socket 对象
				self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				print("在非套接字上进行了操作 重新创建套接字对象")

	def run(self):
		'''
		线程循环
		循环发送和接收msg
		'''
		self.start_connect()
		# 连接成功 错误指示器为 0
		# 开始循环收发
		while self.cnt_flag:
			self.s.send(str.encode(self.identify, 'utf-8'))
			# 收发要实现分离
			try:
				# 对断包需要进行处理 这是取固定字符串长度的问题
				self.msg = self.s.recv(self.maxLen)
				print("recv msg : " + str(self.msg, 'utf-8'))

				# 路径生成以及发送
				# path = PathGen().path_generator()
				# self.s.send(str.encode(path, 'utf-8'))
			# 这里recv一直在等待不超过10长度的数据 等收到之后才会向下运行 否则一致阻塞
			# 每个数据片长度固定是10
			except ConnectionAbortedError:
				self.cnt_flag = False
				print("connction lost")
				time.sleep(1)
				self.start_connect()




