import csv
import configparser
import matplotlib.pyplot as plt
import numpy
import pandas

def readcsv(filename):

	data = pandas.read_csv(filename)

	return data

def writecsv(filename, row):

	# python2可以用file替代open
	with open(filename, 'w', newline='') as csvfile:
		writer = csv.writer(csvfile)

		# 先写入columns_name
		writer.writerow(row)

def readIni(filename):
	config = configparser.ConfigParser()
	config.read(filename, encoding='utf-8')
	# config.get('boata', 'm')
	return config

if __name__ == '__main__':
	#设置使用的字体为支持中文的字体
	plt.rcParams['font.sans-serif']=['SimHei']
	plt.rcParams['axes.unicode_minus'] = False

	data = readcsv('compare.csv')
	plt.plot(data['phi'],data['theta'],'b.')
	plt.xlabel('横倾角($^o$)')
	plt.ylabel('纵倾角($^o$)')
	plt.figure()
	plt.plot(data['rll'],data['pch'],'y.')
	plt.xlabel('横倾角($^o$)')
	plt.ylabel('纵倾角($^o$)')

	plt.figure()

	plt.plot(data['rll'],data['pch'], 'y.', alpha = 0.6, label='实船摇摆角度')
	plt.plot(data['phi'],data['theta'],'b.', alpha = 0.5, label='仿真摇摆角度')
	plt.legend()
	plt.xlabel('横倾角($^o$)')
	plt.ylabel('纵倾角($^o$)')

	plt.figure()
	num_sim = 3000
	sim_t = range(num_sim)
	sim_t = numpy.array(sim_t)*0.1

	num_real = 300
	t = range(num_real)

	plt.plot(sim_t, data['phi'][0:num_sim], '-', label = '仿真俯仰',ms=2, alpha = 0.8)
	plt.plot(t, data['pch'][500:(500+num_real)], '.-', label='实船俯仰', alpha = 0.8)
	plt.ylabel('船体姿态角度($^o$)')
	plt.xlabel('时间($s$)')
	plt.legend()
	plt.figure()
	plt.plot(sim_t, data['theta'][0:num_sim], '-', label = '仿真横摇', alpha = 0.8)
	plt.plot(t, data['rll'][500:(500+num_real)],'.-', label='实船横摇', alpha = 0.8)
	plt.ylabel('船体姿态角度($^o$)')
	plt.xlabel('时间($s$)')
	plt.legend()

	plt.show()

