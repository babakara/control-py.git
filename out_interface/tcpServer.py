# 暂时不用这个模块
import socketserver
import sys


class myTcpServer(socket):
    # 创建 socket 对象

    def __init__(self) -> None:
        super().__init__()
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # 获取本地主机名
        self.host = socket.gethostbyname(socket.gethostname()) 
        self.port = 8712
        # 绑定端口号
        self.serversocket.bind((self.host, self.port))
        # 设置最大连接数，超过后排队
        self.serversocket.listen(10)

    def run(self):
        while True:
            # 建立客户端连接
            clientsocket, addr = self.serversocket.accept()
            print("连接地址: %s" % str(addr))
            msg = '欢迎访问菜鸟教程！' + "\r\n"
            clientsocket.send(msg.encode('utf-8'))
            clientsocket.close()
