import time

import matplotlib.pyplot as plt

import numpy as np
import numpy.matlib
from math import atan2, sin, cos
from algo_struct.algo import *

HEAD = 'heading'
LEFT = 'left_crossing'
RIGHT = 'right_crossing'
OVER = 'overtaking'
OUT = 'out_of_range'


class ORCA:
	def __init__(self, dt=1, terrain=[], tau=35, v_max=5):
		# 接收参数 初始化
		# 类中函数去掉形参 都变成使用私有变量
		self.u_vec = np.zeros(3)
		self.lr = np.zeros(3)
		self.ll = np.zeros(3)
		self.u_point = np.zeros(3, dtype=float)
		self.u_vec = np.zeros(3, dtype=float)
		self.p_a = np.zeros(3, dtype=float)
		self.v_a = np.zeros(3, dtype=float)
		self.p_b_all = []
		self.v_b_all = []
		# self.r_a = r_a
		self.r_a = 0.125 * lim
		# self.r_b = r_b
		self.r_b = 0.125 * lim
		self.tau = tau

		self.terrain = terrain  # 地形约束
		self.v_max = v_max
		self.target = target
		self.v_perf = np.zeros(3)
		self.h_time = dt

	def get_u_orca(self, p_a, p_b, v_opt=np.zeros(3), v_b=np.zeros(3), circle=np.zeros(3)):
		'''
		求VO的u
		转位置坐标系求ORCA的u
		:param v_b:
		:param p_b:
		:param p_a:
		:param v_opt: x,y,z v_opt取 v_a *tau
		:param circle: x,y,r 圆心取p_b + v_b*tau, 半径为r_a + r_b
		:return: u_point,flag
		'''
		# 射线与圆心矢量的夹角
		vc = np.array([circle[0], circle[1], 0])
		c_r = circle[2]
		lvc = norm(p_b - p_a)
		flag = False
		line_vec = np.zeros(3)
		if lvc <= c_r:
			# a进入到VO区域了的处理方法 垂直方向不太好，改成直角锥试试
			# print('lvc小于半径c_r 已经与目标重叠')
			self.u_point = p_a
			self.u_vec = p_a - p_b  # 半平面可行向 半平面法向
			flag = True
			# 根据目标方位和运动趋势 给出方向 障碍在后方应该不处理
			state = self.colreg(vec_a=v_opt, vec_b=v_b, p_b=p_b, p_a=p_a)
			tor = 1
			if state == HEAD or state == RIGHT:
				tor = 1
			else:
				tor = -1
			line_vec = trans(point=np.array([self.u_vec[0], self.u_vec[1], 0, 1]),
			                 angle=np.array([0, 0, tor * (-0.1) * pi]),
			                 mode=False)
			vertical_vec = trans(point=np.array([self.u_vec[0], self.u_vec[1], 0, 1]),
			                     angle=np.array([0, 0, tor * 0.1 * pi]),
			                     mode=False)
			self.u_vec = vertical_vec
			self.ll = get_h_line(self.u_point, line_vec)
			self.lr = self.ll
			return flag

		alpha = asin(c_r / lvc)  # 0-pi VO区域张角的一半
		# v_opt在射线的锐角范围内
		# 圆心矢量右侧r
		# 求右侧射线与圆的交点
		lr_norm = vc * cos(alpha)  # 得到圆心矢量方向 模等于切点矢量模的矢量
		# print('alpha', alpha)
		# print('lr_norm', lr_norm)
		self.lr = trans(point=np.array([lr_norm[0], lr_norm[1], 0, 1]),
		                angle=np.array([0, 0, -alpha]),
		                mode=False)
		# 求左侧射线与圆的交点
		self.ll = trans(point=np.array([lr_norm[0], lr_norm[1], 0, 1]),
		                angle=np.array([0, 0, alpha]),
		                mode=False)

		# 判断v_opt是否在射线之间 以圆心矢量为0度方向
		angle_opt_c = vector_dot_angle(vc, v_opt)
		if abs(angle_opt_c) <= alpha:

			# 以lr为基准轴 如果(v_opt-vc)到基准的角度小于 (ll-vc)的角度 则在扇形内
			# print('v_opt在锥形范围内')
			ll_vc = self.ll[0:3] - vc
			lr_vc = self.lr[0:3] - vc
			v_opt_vc = v_opt - vc

			a_opt = vector_dot_angle(ll_vc, v_opt_vc)
			a_l = vector_dot_angle(ll_vc, lr_vc)
			# 通过叉乘判断v_opt在圆内还是圆外 判断侧 如果在扇形一侧还要满足半径距离约束 排除锥形内扇形外的情况
			if 0 <= a_opt <= a_l:
				if norm(v_opt_vc) <= c_r:
					# print('v_opt在扇形范围内')
					# print('v_opt_vc norm', norm(v_opt_vc))
					# 求过圆心和v_opt的射线与圆弧的交点就是VO边界上到v_opt最近的点
					# u_point = vc + c_r * v_opt_vc / norm(v_opt_vc)
					flag = True
				else:
					# print('不在圆内')
					# 在扇形外 锥形内 靠近原点一侧非VO区域
					flag = False
			else:
				# print('v_opt在扇形外侧 锥形内 远离原点一侧的VO区域')
				# 求v_opt到射线的垂线
				# 先求圆心矢量与y轴的夹角 y轴[0，1]
				flag = True
		else:
			# print('不在锥形内的情况')
			flag = False
		if flag:
			# p_a坐标系下，圆心矢量和y轴夹角
			angle_c = vector_dot_angle(vc, np.array([0, 1, 0]))
			# 海事避碰规则
			state = self.colreg(vec_a=v_opt, vec_b=v_b, p_b=p_b, p_a=p_a)
			#
			if (state == RIGHT) or (state == HEAD):
				# 取右侧边界上的垂足u_point
				# u不是垂直右侧射线就是垂直左侧射线，按这个前提有几何关系可以求出u
				# a点成b,当b是单位矢量时，结果是a投影在b方向上的长度。这个位置就是垂足位置。
				lr_r = v_opt[0] * sin(angle_c + alpha) + v_opt[1] * cos(angle_c + alpha)
				self.u_point[0] = lr_r * sin(angle_c + alpha)
				self.u_point[1] = lr_r * cos(angle_c + alpha)
			elif state == LEFT:
				ll_r = v_opt[0] * sin(angle_c - alpha) + v_opt[1] * cos(angle_c - alpha)
				self.u_point[0] = ll_r * sin(angle_c - alpha)
				self.u_point[1] = ll_r * cos(angle_c - alpha)
			else:
				flag = False
				# print('overtaking do nothing')

			self.u_vec = self.u_point - v_opt
		return flag

	# 改成v_opt_a进入截锥了，走两边的半平面
	def get_orca_area(self, p_a=np.zeros(3), u_point=np.zeros(3), u_vec=np.zeros(3)):
		"""[计算ORCA半平面代数式，转位置坐标系]
		"""
		eta = 1e-5  # 0
		# 已知u
		u_coe = np.zeros(3, dtype=float)
		l_coe = np.zeros(3, dtype=float)

		# 过u点 与u_vec平行直线方程
		if abs(vec_dot(u_vec, np.array([0, 1, 0]))) < eta:
			# 如果u矢量垂直于vx轴
			# print('与纵轴平行 本直线B=0')
			u_coe[0] = 1
			u_coe[1] = 0
			u_coe[2] = u_point[0]
		else:
			# print('与纵轴不平行 B不为0')
			u_coe[0] = u_vec[1] / u_vec[0]
			u_coe[1] = -1
			u_coe[2] = u_point[1] - u_coe[0] * u_point[0]
		# print('u_coe', u_coe)
		# 求过 p_a + u 矢量点的一般方程  这里假设 a b都按ORCA 但实际情况不是 所以不是分摊责任 而是 a负双倍责任
		# point = p_a + 0.5 * u_vec
		point = p_a
		if abs(u_coe[0]) < eta:
			# 如果过u直线与vx平行 那么本直线与lu垂直
			# print(' 与vy平行 B为0')
			l_coe[0] = 1
			l_coe[1] = 0
			l_coe[2] = -point[0]
		else:
			# B不为0
			# print('B不为0')
			l_coe[0] = -1 / u_coe[0]
			l_coe[1] = -1
			l_coe[2] = point[1] - l_coe[0] * point[0]
		re = [l_coe, u_vec, point]
		return re

	def colreg(self, vec_a, vec_b, p_b, p_a, flag='unity'):
		# 根据某种规则选择稳定的避让方向 避免v在VO区正中央引起的左右跳变
		# 由于get_u_orca进行了是否存在碰撞的判断 这里只需要判断p_b相对p_a的方位角
		# 这里使用航向角 而不是艏向角 作为基准方位
		"""[建议避让方向 左/右侧]

		Args:
			vec_a ([ndarray]): [a的速度矢量]
			p_b ([ndarray]): [b当前的位置 不附加速度]
			p_a ([ndarray]): [a的位置]

		Returns:
			[int]: [返回0 1 2 对应左 中 右]]
		"""
		# 目标有4种位置和4种速度
		# 排列组合得出3中处理方式 左 右 保持
		status = []
		# 位置相对角度 顺时针为正
		ori_angle = vector_dot_angle(p_b - p_a, vec_a)
		# status[0] = orientation(ori_angle)

		# 联合判断相对角度
		vec_angle = vector_dot_angle(vec_b, vec_a)
		# 相对速度规则
		vec_angle_rela = vector_dot_angle(vec_a - vec_b, vec_a)
		state = self.orientation(vec_angle_rela)

		# print(state)
		return state

	def orientation(self, ori_angle):
		state = OUT
		angle_t = [20, 130]
		# 位置规则
		if abs(ori_angle) <= angle_t[0] * pi / 180:
			state = HEAD
		elif -angle_t[1] * pi / 180 <= ori_angle < -angle_t[0] * pi / 180:
			state = LEFT
		elif angle_t[0] * pi / 180 < ori_angle <= angle_t[1] * pi / 180:
			state = RIGHT
		elif angle_t[1] * pi / 180 < abs(ori_angle) < pi:
			state = OVER
		else:
			print('why is {}s'.format(ori_angle))
		return state

	# def grapham_scan():
	# 判断采样点在多边形内还是外 n边形顺时针排列n条边 只能用凸包
	# 任意地形都能分解成三角形集合 三角形是凸包 凸包可以用内外检测算法
	# 第一阶段 只考虑凸包情况

	def boat_constraint(self, p_a, maxSpeed, v_a, p_goal, tau, constrants):
		'''
		用采样的方法求可行域  找到距离p_goal最近的可行速度
		:param p_a: bot当前位置
		:param v_a: bot当前速度
		:param p_goal: 目标位置
		:param tau: 无碰时间
		:param H: 所有约束
		:return: v_new新的速度方向  sam可行采样点
		'''
		# 生成约束下的采样点集合
		sam = np.zeros(shape=[80, 20, 4])
		# sam_point = np.zeros(shape=[600, 3])
		angle = (np.random.rand(80) * 270 - 135) * pi / 180  # 左右转角范围 避免急转 rad 30等分
		acc = np.random.rand(20) * maxSpeed + 0.2 * maxSpeed  # 速度限制 避免速度突变 m/s2 10等分
		flag = True
		min_dis = 1000
		v_new = np.zeros(3)
		for i in range(len(angle)):
			for j in range(len(acc)):
				# 运动约束下采样的位置
				tar_vec = v_a * (acc[j] / norm(v_a))
				# tar_vec = np.array([(v_a[0] + acc[j] * dt), (v_a[1] + acc[j] * dt), 0])
				# if norm(tar_vec) > maxSpeed:
				# 	tar_vec = tar_vec * (maxSpeed/norm(tar_vec))
				tar_vec = trans(point=tar_vec, angle=np.array([0, 0, angle[i]]), mode=False)
				piece = p_a + tau * tar_vec
				for sh in constrants:
					flag = False
					# 遍历所有约束 如果满足全部约束 就添加到数组 和v_perf比较夹角
					# 满足动态障碍约束半平面
					flag = in_half_plane(sh, piece)
					# 满足地形障碍约束半平面 地形障碍是多边形 采样点需要在可行域内 不能在地形多边形内部
					if flag == False:
						# 不满足某一个约束
						break
				if flag:
					# 如果这个采样点满足所有约束 记录下来画图
					# print('piece is {}'.format(piece))
					sam[i][j][0:3] = piece
					sam[i][j][3] = norm(piece - p_goal)
					if sam[i][j][3] < min_dis:
						min_dis = sam[i][j][3]
						v_new[0:3] = tar_vec
		return v_new, sam

	def inter_line(self, my_l, axis_range, num=100):
		xx = []
		yy = []
		xx = np.linspace(axis_range[0], axis_range[1], num)
		if abs(my_l[1]) < 1e-5:
			# 直线方程B = 0
			xx = np.zeros(num) - my_l[2]
			xx = np.linspace(axis_range[0], axis_range[1], num)
		else:
			yy = -my_l[0] / my_l[1] * xx + -my_l[2] / my_l[1]
		return xx, yy

	def param_update(self, pa, v_a, p_b_all, v_b_all, target, terrain=[]):
		'''
		更新ORCA避障的USV位置、速度, 障碍位置、速度, USV目标位置。
		:param pa: USV位置
		:param v_a: USV速度
		:param p_b_all: 障碍位置
		:param v_b_all: 障碍速度
		:param target: 目标位置
		:param terrain: 地形
		'''
		# 外部环境信息变化后 刷新内部变量
		self.p_a = pa
		self.v_a = v_a
		self.p_b_all = p_b_all
		self.v_b_all = v_b_all
		self.target = target
		self.terrain = terrain

	def cal_sequence(self):
		# 外部按时间循环，每个时间步按这个计算顺序完成计算
		H = []  # 保存所有半平面

		# 添加航道约束
		if in_or_out_poly(self.p_a, road) and road_flag:
			H.append(np.array([road_l[0], road_l[1], road_l[2], road_l_n[0], road_l_n[1], road_l_n[2]]))
			H.append(np.array([road_r[0], road_r[1], road_r[2], road_r_n[0], road_r_n[1], road_r_n[2]]))

		for i in range(len(self.p_b_all)):
			self.v_perf = 0.5 * self.v_max * (self.target - self.p_a) / norm(self.target - self.p_a)
			self.v_a = self.v_perf
			p_b = self.p_b_all[i]
			v_opt_b = self.v_b_all[i]
			# v_opt = self.v_a * self.tau
			v_opt = self.v_perf * self.tau
			circle = np.array([p_b[0] - self.p_a[0], p_b[1] - self.p_a[1], self.r_a + self.r_b])
			circle += v_opt_b * self.tau
			circle[2] = self.r_a + self.r_b #避免半径被修改
			# u_vec, u_point, uflag, ll, lr = get_u_orca(pa, pb, v_opt=v_opt, circle=circle)
			# 增加偏好速度是 防止 k时刻v_opt不发生碰撞后不会产生半平面，导致选择的k+1时刻速度无法经过半平面筛选，直接选择k-1时刻的v_perf
			uflag = self.get_u_orca(self.p_a, p_b, v_opt=v_opt, v_b=v_opt_b, circle=circle)
			# 返回的结果是相对于p_a的,以p_a为原点的坐标系
			self.u_point = self.u_point + self.p_a  # a坐标系转大地坐标系
			# print('u_vec, u_point, flag is :', u_vec, u_point, uflag)
			# plt.plot(u_point[0], u_point[1], 'o')
			if uflag:
				# print('速度在障碍区域内')
				# f_ll = get_h_line(self.p_a, self.ll)
				# f_lr = get_h_line(self.p_a, self.lr)
				# lx, ly = self.inter_line(f_ll, self.lim)
				# rx, ry = self.inter_line(f_lr, self.lim)
				# plt.plot(lx, ly)
				# plt.plot(rx, ry)

				#  画ORCA半平面
				re = self.get_orca_area(self.p_a, self.u_point, self.u_vec)
				l = re[0]
				# print('半平面直线方程', l)
				orca_l = np.array([l[0], l[1], l[2], self.u_vec[0], self.u_vec[1], self.u_vec[2]])  # 半平面直线和方向矢量
				H.append(orca_l)  # 添加所有的半平面
			# x, y = self.inter_line(l, self.lim)
		# plt.plot(x, y, '--')
		# print('绘制orca平面')

		# 这里有问题 半平面约束的可行方向可能不对。
		# 应该是半平面定义方程有问题
		v_new, record_sim = self.boat_constraint(p_a=self.p_a, maxSpeed=self.v_max, v_a=self.v_a,
		                                         p_goal=self.target, tau=self.tau, constrants=H)
		if norm(v_new) < 0.05:
			v_new = 0.1
			# print('v_new 无解 速度很小不更新')  # v_a为0 有些直线方程求解报错
		else:
			self.v_a = v_new

		return v_new

class BoatSim:
	def __init__(self, h):
		self.boat_u = np.zeros(3)
		self.boat_x = np.zeros(3)
		self.boat_dx = np.zeros(3)
		self.boat_delta = 0
		self.boat_rsp = 0 #可正可负,以安装舷外机的船为例
		self.disb = 0
		self.h = h # 算微分中间量
		self.pidu = np.zeros(2)
		self.oldedx = np.zeros(2)
		self.old2edx = np.zeros(2)

	def kt_equ(self,k):
		# 一阶方程
		ktk = 1
		ktt = 2
		a = 0.001
		out = ktk * (self.boat_delta + self.disb) - k - a*k/ktt
		return out

	def kt_rotation(self):
		# 从boat到ned
		psi = self.boat_x[2]
		rot_matrix = np.array([
			[cos(psi), - sin(psi), 0],
			[sin(psi), cos(psi), 0],
			[0, 0, 1]
		])

		# 更新大地位姿
		self.boat_x = self.boat_x + rot_matrix * self.boat_u *self.h

	def pid_control(self, edx):
		pid1 = [1,0,1]
		pid2 = [1,0,1]
		pid_u1 = pid1[0] * (edx[0] - self.oldedx[0]) + pid1[1] * edx[0] + pid1[2] * (
				edx[0] - 2 * self.oldedx[0] + self.old2edx[0])
		self.pidu[0] = self.pidu[0] + pid_u1
		pid_u2 = pid2[0] * (edx[1] - self.oldedx[1]) + pid2[1] * edx[1] + pid2[2] * (
				edx[1] - 2 * self.oldedx[1] + self.old2edx[1])
		self.pidu[1] = self.pidu[1] + pid_u2

		self.rsp = self.pidu[0]
		self.boat_delta = self.pidu[1]

	def kt_loop(self, next_vec):
		# 主循环, 输入期望速度矢量

		# 更新位姿

		k1 = self.kt_equ(self.boat_u[2])
		k1_ = self.boat_u[2] + 0.5*self.h*k1
		k2 = self.kt_equ(k1_)
		k2_ = self.boat_u[2] + 0.5*self.h*k2
		k3 = self.kt_equ(k2_)
		k3_ = self.boat_u[2] + self.h*k3
		k4 = self.kt_equ(k3_)
		self.boat_u[2] = self.boat_u[2] + (self.h/6.0)*(k1 + 2*k2 + 2*k3 + k4)

		# 更新大地坐标
		self.kt_rotation()
		# 更新转速和舵角

		edx = np.zeros(3)
		b_angle_z = atan2(next_vec[1], next_vec[0])
		beta = atan2(self.boat_u[1], self.boat_u[0])

		edx[0] = norm(next_vec) - norm(self.boat_u[0:1])
		edx[1] = rad_limit(b_angle_z - (self.boat_x[2] + beta))

		self.pid_control(edx)

		# 转速-航速映射关系，正负均有
		self.boat_u[0] = 0.001* self.boat_rsp

# 测试环境1
# 从一个障碍物测试，直到多个障碍物。
# 补充静态障碍物，线性和圆形
# 测试u的求解效果

# 测试环境2
# 单机器人 其他都是障碍 有航道和无航道
if False:
	test = True
	road_flag = False
	if test:
		lim = 300.0  # 限制坐标
		r_a = 0.125 * lim
		r_b = 0.125 * lim
		tau = 35.0
		# a的位置
		pa = np.array([-lim, -lim, 0])
		# 初始速度va
		v_opt_a = np.array([1, 1, 0])

		# b的位置
		pb_all = np.array([[0.6 * lim, 0.68 * lim, 0],
		                   [0.7 * lim, 0.67 * lim, 0],
		                   [0.8 * lim, 0.5 * lim, 0],
		                   [0.8 * lim, -0.7 * lim, 0],
		                   [-0.8 * lim, 0.8 * lim, 0],
		                   [-0.8 * lim, 0.6 * lim, 0],
		                   [0.8 * lim, 0.9 * lim, 0]])
		# 速度vb
		v_opt_b_all = np.array([[-1.85, -1.8, 0],
		                        [-0.6, -0.8, 0],
		                        [-0.2, 1.5, 0],
		                        [-0.2, 1.5, 0],
		                        [1.5, 0, 0],
		                        [1.2, -0.2, 0],
		                        [-0.1, 0.0, 0]])

		# 平行四边形航道约束 机器人在矩形航道内则受两侧航道约束
		# 平行四边形航道左上角点第一个，顺时针取点p1~p4
		road = np.array([[-300, 200, 0],
		                 [0, 200, 0],
		                 [-100, -400, 0],
		                 [-400, -400, 0]])

		road_l = get_h_line(road[0], road[0] - road[3])
		road_l_n = trans(point=road[0] - road[3], angle=np.array([0, 0, -90 * np.pi / 180]), mode=False)

		road_r = get_h_line(road[1], road[1] - road[2])
		road_r_n = trans(point=road[1] - road[2], angle=np.array([0, 0, 90 * np.pi / 180]), mode=False)

		target = np.array([0.95 * lim, 0.95 * lim, 0])
		t_time = 0
		h_time = 1.0
		all_time = 5000.0
		orca = ORCA()
		plt.ion()  # 开启绘图
		while t_time < all_time:

			start_time = time.process_time()
			plt.clf()  # 清除上个绘图

			t_time += h_time

			orca.param_update(pa, v_opt_a, pb_all, v_opt_b_all, target)
			v_opt_a = orca.cal_sequence()
			# 船更新位置
			pa += h_time * v_opt_a
			x_a, y_a = get_circle([pa[0], pa[1]], 0.3 * r_a)

			end_time = time.process_time()
			print('frame time consume is {}'.format(end_time - start_time))

			plt.plot(x_a, y_a)
			# 障碍物更新位置
			pb_all = pb_all + v_opt_b_all * h_time

			plt.plot(target[0], target[1], 'o')
			# print('v_new*tau is ',v_new*tau )
			plt.plot([pa[0], pa[0] + v_opt_a[0] * tau], [pa[1], pa[1] + v_opt_a[1] * tau])

			# for ii in range(len(record_sim)):
			# 	for jj in range(len(record_sim[ii])):
			# 		plt.plot(record_sim[ii][jj][0], record_sim[ii][jj][1], 'x')

			for pb in pb_all:
				x_b, y_b = get_circle([pb[0], pb[1]], 0.3 * r_b)
				plt.plot(x_b, y_b)
			plt.xlim(-1.2 * lim, 1.2 * lim)
			plt.ylim(-1.2 * lim, 1.2 * lim)

			# 绘制航道约束
			if road_flag:
				lx, ly = orca.inter_line(road_l, [-400, -300])
				rx, ry = orca.inter_line(road_r, [-100, 0])
				plt.plot(lx, ly)
				plt.plot(rx, ry)

			# 结束循环
			plt.pause(0.1)  # 暂停一秒
			if norm(target - pa) <= 6:
				print('到达目标了')
				break
			plt.ioff()  # 关闭画图的窗口

# 测试2
# 多机器人  有航道/无航道
if True:
	test = True
	road_flag = False
	if test:
		
		r_a = 10
		r_b = 10
		tau = 5.0

		# 无人艇初始位置
		# b的位置
		space = 60
		n = 8
		m = 8
		swarms = 3
		lim = space* 2.5 * n  # 限制坐标
		
		robot_q = np.zeros((n*m,3), dtype=float)
		# print(robot_p.shape[0])
		# swarm 1
		for i in range(n):
			for j in range(m):
				# print("i is {}, j is {}, id is {}, xy is {} "
				# .format(i, j, i*m + j, [i*space, j*space, 0]))
				robot_q[i*m + j] = [i*space, j*space, 0]

		# print(robot_q)
		# swarm 2
		robot_r = np.zeros((n*m,3), dtype=float)
		for i in range(robot_q.shape[0]):
			robot_r[i][0] = robot_q[i][0] + lim - n*space
			robot_r[i][1] = robot_q[i][1] + lim - m*space
		# print(robot_r)
		# swarm 3
		robot_s = np.zeros((n*m,3), dtype=float)
		for i in range(robot_q.shape[0]):
			robot_s[i][0] = robot_q[i][0]
			robot_s[i][1] = robot_q[i][1] + lim - m*space
		# print(robot_s)
		# swarm 4
		robot_t = np.zeros((n*m,3), dtype=float)
		for i in range(robot_q.shape[0]):
			robot_t[i][0] = robot_q[i][0] + lim - n*space
			robot_t[i][1] = robot_q[i][1]
		# print(robot_t)
		# 拼接三个群
		temp = np.vstack((robot_q, robot_r))
		# print(temp)
		robot_p = np.vstack((temp, robot_s))

		# print(robot_p)
		# print(robot_p.shape[0])

		# b的位置
		target = np.concatenate((robot_r,robot_q,robot_t),axis=0)

		# 速度vb
		robot_v = np.zeros((swarms * n* m, 3), dtype=float)

		# 平行四边形航道约束 机器人在矩形航道内则受两侧航道约束
		# 平行四边形航道左上角点第一个，顺时针取点p1~p4
		road = np.array([[-300, 200, 0],
		                 [0, 200, 0],
		                 [-100, -400, 0],
		                 [-400, -400, 0]])

		road_l = get_h_line(road[0], road[0] - road[3])
		road_l_n = trans(point=road[0] - road[3], angle=np.array([0, 0, -90 * np.pi / 180]), mode=False)

		road_r = get_h_line(road[1], road[1] - road[2])
		road_r_n = trans(point=road[1] - road[2], angle=np.array([0, 0, 90 * np.pi / 180]), mode=False)

		t_time = 0
		h_time = 0.5
		all_time = 500.0

		# 船体模型
		orca = ORCA(tau=tau)
		orca.r_a = 1.5*r_a
		orca.r_b = 1.5*r_b
		plt.ion()  # 开启绘图
		record_path = list()

		frame = 0
		total_time = 0
		while 1:

			start_time = time.process_time()
			plt.clf()  # 清除上个绘图

			t_time += h_time
			for i in range(len(robot_p)):
				objs = np.delete(robot_p, i, axis=0)
				objs_v = np.delete(robot_v, i, axis=0)
				orca.param_update(robot_p[i], robot_v[i], objs, objs_v, target[i])
				robot_v[i] = orca.cal_sequence()
			# 船更新位置
			# 直接运动学更新
			robot_p = robot_p + robot_v * h_time

			# 选择K-T模型更新
			# 期望速度和航向输入, PD控制跟踪

			# x_a, y_a = get_circle([robot_p[0][0], robot_p[0][1]], r_a)
			frame = frame + 1
			end_time = time.process_time()
			total_time = total_time + end_time - start_time
			print('frame time consume is {}, mean is {}'.format(end_time - start_time, total_time/frame))

			for pb in robot_p:

				# x_b, y_b = get_circle([pb[0], pb[1]], r_b)
				# plt.plot(x_b, y_b)
				draw_circle = plt.Circle((pb[0], pb[1]), r_b, color='grey')

				plt.gcf().gca().add_artist(draw_circle)

			plt.xlim(-0.1 * lim, 1.1 * lim)
			plt.ylim(-0.1 * lim, 1.1 * lim)

			# 绘制航道约束
			if road_flag:
				lx, ly = orca.inter_line(road_l, [-400, -300])
				rx, ry = orca.inter_line(road_r, [-100, 0])
				plt.plot(lx, ly)
				plt.plot(rx, ry)
			plt.xlabel('x / m')
			plt.ylabel('y / m')
			# 结束循环
			# plt.pause(0.1)  # 暂停一秒

			plt.savefig('save_fig_192/' +str(frame) + '.jpg')
			flag = True
			for i in range(len(robot_p)):
				# print(norm(target[i] - robot_p[i]))
				if norm(target[i] - robot_p[i]) > 6:
					flag = False
			if flag:
				print('都到达目标了')
				# plt.pause(10) # 暂停
				break
		plt.ioff()  # 关闭画图的窗口
